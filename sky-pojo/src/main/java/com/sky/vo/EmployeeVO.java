package com.sky.vo;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "员工返显数据格式")
public class EmployeeVO implements Serializable {
    private Long id;

    private String username;

    private String name;

    private String phone;

    private String sex;

    private String idNumber;
}
