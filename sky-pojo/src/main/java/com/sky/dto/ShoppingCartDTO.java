package com.sky.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;

@Data
@ApiModel(description = "添加或删除传递的数据模型")
public class ShoppingCartDTO implements Serializable {

    @ApiModelProperty("菜品id")
    private Long dishId;
    @ApiModelProperty("套餐id")
    private Long setmealId;
    @ApiModelProperty("菜品口味")
    private String dishFlavor;

}
