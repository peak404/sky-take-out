package com.sky.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel("分页查询传递的数据模型")
public class DishPageQueryDTO implements Serializable {

    @ApiModelProperty("页码")
    private int page;
    @ApiModelProperty("每页条数")
    private int pageSize;
    @ApiModelProperty("姓名")
    private String name;
    @ApiModelProperty("分类id")
    //分类id
    private Integer categoryId;
    @ApiModelProperty("状态")
    //状态 0表示禁用 1表示启用
    private Integer status;

}
