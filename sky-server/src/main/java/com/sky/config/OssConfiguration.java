package com.sky.config;

import com.sky.utils.AliOSSUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class OssConfiguration {

    //自定义阿里云bean对象
    @Bean
    @ConditionalOnMissingBean
    public AliOSSUtils aliOssUtil(){
        log.info("创建阿里云文件上传工具类对象");
        return new AliOSSUtils();
    }



}
