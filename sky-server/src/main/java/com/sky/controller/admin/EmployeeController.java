package com.sky.controller.admin;

import com.sky.constant.JwtClaimsConstant;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.properties.JwtProperties;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.EmployeeService;
import com.sky.utils.JwtUtil;
import com.sky.vo.EmployeeLoginVO;
import com.sky.vo.EmployeeVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 员工管理
 */
@RestController
@RequestMapping("/admin/employee")
@Slf4j
@Api(tags = "员工控制器")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private JwtProperties jwtProperties;

    /**
     * 登录
     *
     * @param employeeLoginDTO
     * @return
     */
    @PostMapping("/login")
    @ApiOperation("员工登录")
    public Result<EmployeeLoginVO> login(@RequestBody EmployeeLoginDTO employeeLoginDTO) {
        log.info("员工登录：{}", employeeLoginDTO);

        Employee employee = employeeService.login(employeeLoginDTO);

        //登录成功后，生成jwt令牌
        Map<String, Object> claims = new HashMap<>();
        claims.put(JwtClaimsConstant.EMP_ID, employee.getId());
        String token = JwtUtil.createJWT(
                jwtProperties.getAdminSecretKey(),//密钥
                jwtProperties.getAdminTtl(),//存活时间
                claims);//载荷

        //通过builder创建返回给前端的VO模型(不需要new)
        EmployeeLoginVO employeeLoginVO = EmployeeLoginVO.builder()
                .id(employee.getId())
                .userName(employee.getUsername())
                .name(employee.getName())
                .token(token)
                .build();

        return Result.success(employeeLoginVO);
    }

    /**
     * 退出
     * @return
     */
    @ApiOperation("员工退出")
    @PostMapping("/logout")
    public Result<String> logout() {
        return Result.success();
    }


    /**
     * 新增员工
     * @param employeeDTO
     * @return
     */
    @PostMapping
    @ApiOperation("新增员工")
    public Result<String> add(@RequestBody EmployeeDTO employeeDTO){
        //log.info("[Controller线程id:{}]",Thread.currentThread().getId());

        log.info("[新增员工 employeeDTO:{}]",employeeDTO);

        Integer tag = employeeService.add(employeeDTO);

        return tag>0? Result.success("添加成功"):Result.error("添加失败");
    }


    /**
     * 分页查询
     * @param employeePageQueryDTO name模糊 page页码 pageSize每页展示条数
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public Result<PageResult> findByPage(EmployeePageQueryDTO employeePageQueryDTO){
        log.info("[分页条件查询 employeePageQueryDTO:{}]",employeePageQueryDTO);
        PageResult employeeList = employeeService.findByPage(employeePageQueryDTO);
        return Result.success(employeeList);
    }

    /**
     * 启用禁用员工
     * @param status 状态
     * @param id id
     * @return
     */
    @ApiOperation("启用禁用")
    @PostMapping("/status/{status}")
    public Result<String> modifyStatus(@PathVariable Integer status,Long id){
        log.info("[启用禁用 status:{} id:{}]",status,id);
        Integer tag = employeeService.modifyStatus(status,id);
        return tag>0? Result.success("修改成功"):Result.error("修改失败");
    }


    /**
     * 根据id查询员工
     * @param id
     * @return
     */
    @ApiOperation("id查询")
    @GetMapping("{id}")
    public Result<EmployeeVO> getById(@PathVariable Integer id){
        log.info("[根据id查询员工 id:{}]",id);
        EmployeeVO employeeVO = employeeService.getById(id);
        return Result.success(employeeVO);
    }

    /**
     * 修改员工
     * @param employeeDTO
     * @return
     */
    @ApiOperation("修改员工")
    @PutMapping
    public Result<String> modifyEmp(@RequestBody EmployeeDTO employeeDTO){
        log.info("[修改员工 employeeDTO:{}]",employeeDTO);
        Integer tag = employeeService.modifyEmp(employeeDTO);
        return tag>0?Result.success("修改成功"):Result.error("修改失败");
    }




}
