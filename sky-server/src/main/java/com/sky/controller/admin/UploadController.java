package com.sky.controller.admin;

import com.sky.constant.MessageConstant;
import com.sky.result.Result;
import com.sky.utils.AliOSSUtils;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;

@RestController
@Api(tags = "文件上传控制器")
@Slf4j
@RequestMapping("/admin/common")
public class UploadController {

    @Resource
    private AliOSSUtils aliOssUtil;

    @PostMapping("upload")
    public Result<String> upload(MultipartFile file){
        //调用工具类上传到阿里云，获取到url路径
        String url=null;
        try {
            url = aliOssUtil.upload(file);
        } catch (IOException e) {
            e.printStackTrace();
            return Result.error(MessageConstant.UPLOAD_FAILED);
        }

        //响应
        return Result.success(url);
    }
}
