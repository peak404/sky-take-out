package com.sky.controller.admin;

import com.sky.constant.StatusConstant;
import com.sky.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController("adminShopController")
@Slf4j
@Api(tags = "店铺控制器")
@RequestMapping("/admin/shop")
public class ShopController {

    @Resource
    private RedisTemplate redisTemplate;

    @ApiOperation("设置店铺营业状态")
    @PutMapping("/{status}")
    public Result<String> setStatus(@PathVariable Integer status) {
        log.info("[设置店铺营业状态为:{}]", status == 1 ? "营业中" : "打烊中");
        redisTemplate.opsForValue().set(StatusConstant.KEY,status);
        return Result.success();
    }

    @ApiOperation("获取店铺营业状态")
    @GetMapping("/status")
    public Result<Integer> getStatus(){
        Object status = redisTemplate.opsForValue().get(StatusConstant.KEY);
        log.info("[获取店铺营业状态为:{}]",status);
        //如果不等于null在进行强转
        return status!=null?Result.success((Integer) status):Result.success(0);
    }
}
