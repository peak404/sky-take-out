package com.sky.controller.admin;

import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.SetmealService;
import com.sky.vo.SetmealVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@Slf4j
@Api(tags = "套餐管理器")
@RequestMapping("/admin/setmeal")
public class SetmealController {

    @Resource
    private SetmealService setmealService;


    /**
     * 新增套餐
     * @param setmealDTO
     * @return
     */
    @PostMapping
    @ApiOperation("新建套餐")
    public Result<String> add(@RequestBody SetmealDTO setmealDTO){
        log.info("[新增套餐 DTO:{}]",setmealDTO);
        Integer tag = setmealService.add(setmealDTO);
        return tag>0?Result.success("新增成功"): Result.error("新增失败");
    }


    /**
     * 分页查询
     * @param setmealPageQueryDTO
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("分页查询")
    public Result<PageResult> findByPage(SetmealPageQueryDTO setmealPageQueryDTO){
        log.info("[分页查询 page:{}]",setmealPageQueryDTO);
        PageResult pageResult = setmealService.findByPage(setmealPageQueryDTO);
        return Result.success(pageResult);
    }


    /**
     * 批量删除套餐
     * @param ids
     * @return
     */
    @DeleteMapping
    @ApiOperation("删除套餐")
    public Result<String> removeByIds(@RequestParam List<Long> ids){
        log.info("[删除套餐 ids:{}]",ids);
        Integer tag = setmealService.removeByIds(ids);
        return tag>0?Result.success("删除成功"):Result.error("删除失败");
    }


    /**
     * 根据id查询套餐
     * @param id
     * @return
     */
    @ApiOperation("根据id查询套餐")
    @GetMapping("{id}")
    public Result<SetmealVO> getById(@PathVariable Long id){
        log.info("[根据id查询套餐 id:{}]",id);
        SetmealVO setmealVO = setmealService.getById(id);
        return Result.success(setmealVO);
    }

    /**
     * 修改套餐
     * @param setmealDTO
     * @return
     */
    @PutMapping
    @ApiOperation("修改套餐")
    public Result<String> modifySetmeal(@RequestBody SetmealDTO setmealDTO){
        log.info("[修改套餐 setmealDTO:{}]",setmealDTO);
        Integer tag = setmealService.modifySetmeal(setmealDTO);
        return tag>0?Result.success("修改成功"):Result.error("修改失败");
    }


    /**
     * 套餐停售启售
     * @param status
     * @param id
     * @return
     */
    @ApiOperation("停售启售")
    @PostMapping("/status/{status}")
    public Result<String> modifyStatus(@PathVariable Integer status,Long id){
        log.info("[套餐启售停售 status:{} id:{}]",status,id);
        Integer tag = setmealService.modifyStatus(status,id);
        return tag>0?Result.success("修改成功"):Result.error("修改失败");
    }




}
