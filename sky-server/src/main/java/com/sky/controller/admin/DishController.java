package com.sky.controller.admin;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Api(tags = "菜品控制器")
@RestController
@Slf4j
@RequestMapping("/admin/dish")
public class DishController {

    @Resource
    private DishService dishService;

    /**
     * 新增菜品
     * @param dishDTO
     * @return
     */
    @ApiOperation("新增菜品")
    @PostMapping
    public Result<String> addDish(@RequestBody DishDTO dishDTO){
        log.info("[新增菜品] dishDTO:{}",dishDTO);
        dishService.addDish(dishDTO);
        return Result.success();
    }


    /**
     * 分页查询菜品
     * @param dishPageQueryDTO
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public Result<PageResult> findByPage(DishPageQueryDTO dishPageQueryDTO){
        log.info("[分页查询 dto:{}]",dishPageQueryDTO);
        PageResult pageResult = dishService.findByPage(dishPageQueryDTO);
        return Result.success(pageResult);
    }

    /**
     * 删除菜品
     * @param ids
     * @return
     */
    @ApiOperation("删除菜品")
    @DeleteMapping
    public Result<String> removeByIds(@RequestParam List<Long> ids){
        log.info("[删除菜品 ids:{}]",ids);
        Integer tag = dishService.removeById(ids);
        return tag>0?Result.success("删除成功"):Result.error("删除失败");
    }


    /**
     * 根据id查询菜品
     * @param id
     * @return 单条数据
     */
    @GetMapping("/{id}")
    @ApiOperation("根据id查询菜品")
    public Result<DishVO> getById(@PathVariable Long id){
        log.info("[根据id查询菜品 id:{}]",id);
        DishVO dishVO = dishService.getById(id);
        return Result.success(dishVO);
    }


    //修改
    @ApiOperation("修改菜品")
    @PutMapping
    public Result<String> modifyDish(@RequestBody DishDTO dishDTO){
        log.info("[修改菜品 dishDTO:{}]",dishDTO);
        Integer tag = dishService.modifyDish(dishDTO);
        return tag>0? Result.success("修改成功"):Result.error("修改失败");
    }

    @ApiOperation("修改菜品状态")
    @PostMapping("/status/{status}")
    public Result<String> modifyStatus(@PathVariable Integer status,Long id){
        log.info("[启售停售菜品 status:{} id:{}]",status,id);
        Integer tag = dishService.modifyStatus(status,id);
        return tag>0?Result.success("修改成功"):Result.error("修改失败");
    }

    @ApiOperation("根据分类id查询菜品")
    @GetMapping("/list")
    public Result<List<DishVO>> findByCategoryId(Long categoryId){
        log.info("根据分类id查询菜品 categoryId:{}",categoryId);
        List<DishVO> dishVOList = dishService.findByCategoryId(categoryId);
        return Result.success(dishVOList);
    }



}
