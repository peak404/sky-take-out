package com.sky.controller.user;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.sky.constant.StatusConstant;
import com.sky.entity.Dish;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController("userDishController")
@Slf4j
@RequestMapping("/user/dish")
@Api(tags = "C端-菜品控制器")
public class DishController {

    @Resource
    private DishService dishService;

    /**
     * 根据分类id查询菜品
     * @param categoryId
     * @return
     */
    @ApiOperation("根据分类id查询菜品")
    @GetMapping("/list")
    public Result<List<DishVO>> findByCategoryId(Long categoryId){
        log.info("[根据分类id查询菜品 categoryId:{}]",categoryId);

        //用户端只展示启售的菜品
        Dish dish = new Dish();
        dish.setCategoryId(categoryId);
        dish.setStatus(StatusConstant.ENABLE);

        List<DishVO> dishVOList = dishService.findByCategoryIdWithFlavor(dish);
        return Result.success(dishVOList);
    }

}
