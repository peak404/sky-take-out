package com.sky.controller.user;

import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;
import com.sky.result.Result;
import com.sky.service.ShoppingCartService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/user/shoppingCart")
@Api(tags = "C端-购物车控制器")
public class ShoppingCartController {

    @Resource
    private ShoppingCartService shoppingCartService;

    /**
     * 添加购物车
     * @param shoppingCartDTO 包含dishId setmealId dishFlavor
     * @return
     */
    @ApiOperation("添加购物车")
    @PostMapping("/add")
    public Result<String> add(@RequestBody ShoppingCartDTO shoppingCartDTO) {
        log.info("[添加购物车 dto:{}]", shoppingCartDTO);
        Integer tag = shoppingCartService.add(shoppingCartDTO);
        return tag > 0 ? Result.success("添加成功") : Result.error("添加失败");
    }

    /**
     * 查看购物车
     * @return
     */
    @ApiOperation("查看购物车")
    @GetMapping("/list")
    public Result<List<ShoppingCart>> show() {
        log.info("[查看购物车]");
        List<ShoppingCart> shoppingCartList = shoppingCartService.show();
        return Result.success(shoppingCartList);
    }

    /**
     * 清空购物车
     * @return
     */
    @ApiOperation("清空购物车")
    @DeleteMapping("/clean")
    public Result<String> removeAll(){
        log.info("[清空购物车]");
        Integer tag = shoppingCartService.removeAll();
        return tag>0?Result.success("清空成功"):Result.error("清空失败");
    }

    /**
     * 删除购物车中的一个商品
     * @param shoppingCartDTO
     * @return
     */
    @ApiOperation("删除购物车中的一个商品")
    @PostMapping("/sub")
    public Result<String> removeOne(@RequestBody ShoppingCartDTO shoppingCartDTO){
        log.info("[删除购物车中的一个商品]");
        Integer tag =  shoppingCartService.removeOne(shoppingCartDTO);
        return tag>0?Result.success("删除成功"):Result.error("删除失败");
    }
}
