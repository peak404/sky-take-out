package com.sky.controller.user;

import com.sky.constant.StatusConstant;
import com.sky.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController("userShopController")
@Slf4j
@Api(tags = "店铺控制器")
@RequestMapping("/user/shop")
public class ShopController {

    @Resource
    public RedisTemplate redisTemplate;


    @ApiOperation("获取店铺营业状态")
    @GetMapping("/status")
    public Result<Integer> getStatus() {
        Object status = redisTemplate.opsForValue().get(StatusConstant.KEY);
        log.info("[获取店铺营业状态为:{}]", status);
        return status != null ? Result.success((Integer) status) : Result.success(0);

    }


}
