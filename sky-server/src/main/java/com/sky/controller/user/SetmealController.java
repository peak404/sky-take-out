package com.sky.controller.user;

import com.sky.entity.Setmeal;
import com.sky.result.Result;
import com.sky.service.SetmealService;
import com.sky.vo.DishItemVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController("userSetmealController")
@Slf4j
@RequestMapping("/user/setmeal")
@Api(tags = "C端-套餐控制器")
public class SetmealController {

    @Resource
    private SetmealService setmealService;

    /**
     * 根据分类id查询套餐
     * @param categoryId
     * @return
     */
    @ApiOperation("根据分类id查询套餐")
    @GetMapping("/list")
    public Result<List<Setmeal>> findByCategoryId(Long categoryId) {
        log.info("[根据分类id查询套餐 categoryId:{}]", categoryId);
        List<Setmeal> setmealList = setmealService.findByCategoryId(categoryId);
        return Result.success(setmealList);
    }

    /**
     * 根据套餐id查询菜品
     * @param id 套餐id
     * @return
     */
    @ApiOperation("根据套餐id查找菜品")
    @GetMapping("/dish/{id}")
    public Result<List<DishItemVO>> findDishBySetmealId(@PathVariable Long id) {
        log.info("[根据套餐id查询菜品 setmealId:{}]", id);
        List<DishItemVO> dishItemVOList = setmealService.findBySetmealId(id);
        return Result.success(dishItemVOList);
    }


}
