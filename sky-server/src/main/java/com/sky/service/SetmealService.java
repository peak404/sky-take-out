package com.sky.service;


import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.result.PageResult;
import com.sky.vo.DishItemVO;
import com.sky.vo.SetmealVO;

import java.util.List;

public interface SetmealService {

    //新增套餐
    Integer add(SetmealDTO setmealDTO);

    //分页查询
    PageResult findByPage(SetmealPageQueryDTO setmealPageQueryDTO);

    //删除套餐
    Integer removeByIds(List<Long> ids);

    //根据id查询套餐
    SetmealVO getById(Long id);

    //修改套餐
    Integer modifySetmeal(SetmealDTO setmealDTO);

    //启售停售
    Integer modifyStatus(Integer status, Long id);

    //根据分类id查询套餐
    List<Setmeal> findByCategoryId(Long categoryId);

    //根据套餐id查询套餐
    List<DishItemVO> findBySetmealId(Long categoryId);
}
