package com.sky.service;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.vo.DishVO;

import java.util.List;

public interface DishService {

    //新增菜品
    void addDish(DishDTO dishDTO);

    //分页查询
    PageResult findByPage(DishPageQueryDTO dishPageQueryDTO);

    //删除菜品
    Integer removeById(List<Long> ids);

    //根据id查询菜品
    DishVO getById(Long id);

    //修改菜品
    Integer modifyDish(DishDTO dishDTO);

    //修改状态
    Integer modifyStatus(Integer status, Long id);

    //根据分类id查询菜品
    List<DishVO> findByCategoryId(Long categoryId);

    //根据分类id查询菜品(带口味)
    List<DishVO> findByCategoryIdWithFlavor(Dish dish);
}
