package com.sky.service.impl;

import com.sky.context.BaseContext;
import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.ShoppingCart;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.mapper.ShoppingCartMapper;
import com.sky.service.SetmealService;
import com.sky.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class ShoppingCartServiceImpl implements ShoppingCartService {

    @Resource
    private ShoppingCartMapper shoppingCartMapper;
    @Resource
    private DishMapper dishMapper;
    @Resource
    private SetmealMapper setmealMapper;

    //添加购物车
    @Override
    public Integer add(ShoppingCartDTO shoppingCartDTO) {
        Integer tag = 0;
        //转换为对应实体类型
        ShoppingCart shoppingCart = new ShoppingCart();
        BeanUtils.copyProperties(shoppingCartDTO, shoppingCart);
        //获取当前操作用户的id
        Long userId = BaseContext.getCurrentId();
        shoppingCart.setUserId(userId);

        //判断当前添加的数据(菜品/套餐) 是否存在
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.select(shoppingCart);
        if (shoppingCartList != null && shoppingCartList.size() > 0) {
            //存在 , 将要添加的数据的份数+1
            //其实查询到就是一条数据
            shoppingCart = shoppingCartList.get(0);
            //基于id更新数据
            shoppingCart.setNumber(shoppingCart.getNumber() + 1);
            tag = shoppingCartMapper.update(shoppingCart);
            log.info("aaa");
        } else {
            //不存在,看具体要添加什么
            Long dishId = shoppingCartDTO.getDishId();
            if (dishId != null) {
                //菜品 -> 冗余字段
                //shoppingCart目前中有userid dishid dishflavor
                //根据dishId查找该菜品基本信息
                Dish dish = dishMapper.selectById(dishId);
                shoppingCart.setName(dish.getName());
                shoppingCart.setImage(dish.getImage());
                shoppingCart.setAmount(dish.getPrice());
            } else {
                //套餐 -> 冗余字段
                Long setmealId = shoppingCartDTO.getSetmealId();
                //根据套餐id查询套餐基本信息
                Setmeal setmeal = setmealMapper.selectById(setmealId);
                shoppingCart.setName(setmeal.getName());
                shoppingCart.setImage(setmeal.getImage());
                shoppingCart.setAmount(setmeal.getPrice());
            }
            //设置份数和创建时间
            shoppingCart.setNumber(1);
            shoppingCart.setCreateTime(LocalDateTime.now());
            log.info("[添加商品为:{}]", shoppingCart);
            //插入数据库
            tag = shoppingCartMapper.insert(shoppingCart);
        }
        return tag;
    }

    //查看购物车
    @Override
    public List<ShoppingCart> show() {
        //获取当前操作用户的id
        ShoppingCart shoppingCart = ShoppingCart.builder()
                .userId(BaseContext.getCurrentId())
                .build();
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.select(shoppingCart);
        return shoppingCartList;
    }

    //清空购物车
    @Override
    public Integer removeAll() {
        ;
        return shoppingCartMapper.deleteAll(BaseContext.getCurrentId());
    }

    //删除购物车中的一个商品
    @Override
    public Integer removeOne(ShoppingCartDTO shoppingCartDTO) {
        Integer tag = 0;
        ShoppingCart shoppingCart = new ShoppingCart();
        BeanUtils.copyProperties(shoppingCartDTO, shoppingCart);
        shoppingCart.setUserId(BaseContext.getCurrentId());

        //根据菜品id,口味  或者 套餐id进行查询
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.select(shoppingCart);
        //非空判断
        if (shoppingCartList != null && shoppingCartList.size() > 0) {
            shoppingCart = shoppingCartList.get(0);
            //判断当前商品是一份还是多份
            if (shoppingCart.getNumber() == 1) {
                //一份,直接删除
                tag = shoppingCartMapper.deleteOneById(shoppingCart.getId());
            } else {
                //多份,份数-1
                shoppingCart.setNumber(shoppingCart.getNumber() - 1);
                //更新
                tag = shoppingCartMapper.update(shoppingCart);
            }
        }
        return tag;
    }
}
