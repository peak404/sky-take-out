package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.exception.SetmealEnableFailedException;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.SetmealService;
import com.sky.vo.DishItemVO;
import com.sky.vo.DishVO;
import com.sky.vo.SetmealVO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class SetmealServiceImpl implements SetmealService {

    @Resource
    private SetmealMapper setmealMapper;
    @Resource
    private SetmealDishMapper setmealDishMapper;
    @Resource
    private DishMapper dishMapper;

    //新增套餐
    @Override
    @Transactional(rollbackFor = SQLException.class)
    public Integer add(SetmealDTO setmealDTO) {
        //转换为对应实体类型
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealDTO, setmeal);

        //先插入套餐基本属性
        //默认初始状态为停售
        setmeal.setStatus(StatusConstant.DISABLE);
        Integer tag = setmealMapper.insert(setmeal);

        //在插入套餐中关联的菜品
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        //获取到插入套餐的id
        Long setmealId = setmeal.getId();
        //往套餐菜品关系表中添加数据
        for (SetmealDish setmealDish : setmealDishes) {
            //设置套餐id
            setmealDish.setSetmealId(setmealId);
        }
        //批量插入
        setmealDishMapper.insertBySetmealId(setmealDishes);

        return tag;
    }

    //分页查询
    @Override
    public PageResult findByPage(SetmealPageQueryDTO setmealPageQueryDTO) {
        //分页
        PageHelper.startPage(setmealPageQueryDTO.getPage(), setmealPageQueryDTO.getPageSize());
        //调用
        List<SetmealVO> setmealList = setmealMapper.selectByPage(setmealPageQueryDTO);
        Page<SetmealVO> p = (Page<SetmealVO>) setmealList;
        return new PageResult(p.getTotal(), p.getResult());
    }

    //删除套餐
    @Override
    @Transactional(rollbackFor = SQLException.class)
    public Integer removeByIds(List<Long> ids) {
        //根据id查询状态
        for (Long id : ids) {
            Integer status = setmealMapper.selectStatusByIds(id);
            if (status == StatusConstant.ENABLE) {
                //启售中的套餐不可删除
                throw new DeletionNotAllowedException(MessageConstant.SETMEAL_ON_SALE);
            }
        }

        //删除套餐
        Integer tag = setmealMapper.deleteByIds(ids);
        //删除该套餐关联的菜品
        setmealDishMapper.deleteBySetmealId(ids);

        return tag;
    }

    //数据回显
    @Override
    public SetmealVO getById(Long id) {
        //根据套餐id获取该套餐基本信息
        Setmeal setmeal = setmealMapper.selectById(id);

        //根据套餐id获取该套餐关联的菜品列表
        List<SetmealDish> setmealDishes = setmealDishMapper.selectDishesBySetmealId(id);

        //一起封装到VO模型中
        SetmealVO setmealVO = new SetmealVO();
        BeanUtils.copyProperties(setmeal, setmealVO);
        setmealVO.setSetmealDishes(setmealDishes);

        return setmealVO;
    }

    //修改套餐
    @Override
    public Integer modifySetmeal(SetmealDTO setmealDTO) {
        //修改套餐表中的基本属性
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealDTO, setmeal);
        Integer tag = setmealMapper.update(setmeal);

        //修改套餐中关联的套餐
        //先删除原来套餐表关联的所有菜品
        //获取当前套餐id
        List<Long> setmealId = List.of(setmeal.getId());
        setmealDishMapper.deleteBySetmealId(setmealId);

        //在重新提交,进行保存  (从DTO模型中获取菜品)
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        for (SetmealDish setmealDish : setmealDishes) {
            //设置当前id
            setmealDish.setSetmealId(setmeal.getId());
        }
        //添加
        setmealDishMapper.insertBySetmealId(setmealDishes);

        return tag;
    }

    //启售停售
    @Override
    public Integer modifyStatus(Integer status, Long id) {
        //启售套餐时候,判断套餐中是否有停售菜品,套餐内如果有停售菜品，则套餐无法上架
        if (status == StatusConstant.ENABLE) {
            //表示执行的是启售操作
            //根据id获取包含的菜品信息
            List<Dish> dishList = dishMapper.selectBySetmealId(id);
            //集合非空判断
            if (dishList.size() > 0 && dishList != null) {
                //判断菜品集合中的状态
                dishList.forEach(dish -> {
                    if (dish.getStatus() == StatusConstant.DISABLE) {
                        //菜品未启用 -> 抛出异常(套餐内包含未启售菜品，无法启售)
                        throw new SetmealEnableFailedException(MessageConstant.SETMEAL_ENABLE_FAILED);
                    }
                });
            }
        }

        //构建setmeal对象
        Setmeal setmeal = Setmeal.builder()
                .id(id)
                .status(status)
                .build();
        return setmealMapper.update(setmeal);
    }

    //根据分类id查询套餐
    @Override
    public List<Setmeal> findByCategoryId(Long categoryId) {
        Setmeal setmeal = Setmeal.builder()
                .categoryId(categoryId)
                .status(StatusConstant.ENABLE)
                .build();
        List<Setmeal> setmealList = setmealMapper.selectByCategoryId(setmeal);
        return setmealList;
    }

    //根据套餐id查询套餐
    @Override
    public List<DishItemVO> findBySetmealId(Long categoryId) {
        //查询该套餐包含的菜品
        List<SetmealDish> setmealDishList = setmealDishMapper.selectDishesBySetmealId(categoryId);
        List<DishItemVO> dishItemVOList = new ArrayList<>();

        for (SetmealDish setmealDish : setmealDishList) {
            //从集合中获取名称和份数
            String name = setmealDish.getName();
            Integer copies = setmealDish.getCopies();
            Long dishId = setmealDish.getDishId();
            //根据菜品id去查找菜品信息
            Dish dish = dishMapper.selectById(dishId);
            String image = dish.getImage();
            String description = dish.getDescription();
            //封装为VO模型
            DishItemVO dishItemVO = DishItemVO.builder()
                    .name(name)
                    .copies(copies)
                    .image(image)
                    .description(description)
                    .build();
            //添加到集合中
            dishItemVOList.add(dishItemVO);
        }
        return dishItemVOList;
    }


}
