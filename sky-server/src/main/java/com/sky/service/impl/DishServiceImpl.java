package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.mapper.DishFlavorMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealDishMapper;
import com.sky.result.PageResult;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@Slf4j
public class DishServiceImpl implements DishService {

    @Resource
    private DishMapper dishMapper;
    @Resource
    private DishFlavorMapper dishFlavorMapper;
    @Resource
    private SetmealDishMapper setmealDishMapper;
    @Resource
    private RedisTemplate redisTemplate;

    //新增菜品
    @Override
    @Transactional//交给事务管理,涉及到菜品和口味,同时成功,同时失败
    public void addDish(DishDTO dishDTO) {
        //转换为对应实体类
        Dish dish = new Dish();
        BeanUtils.copyProperties(dishDTO, dish);

        dish.setStatus(StatusConstant.DISABLE);
        //添加菜品到菜品表
        dishMapper.insertDish(dish);

        //添加口味到口味表
        //新增成功后,获取当前菜品的id
        Long dishId = dish.getId();
        log.info("[新增完毕 菜品id:{}]", dishId);

        //基于id新增口味数据
        List<DishFlavor> flavors = dishDTO.getFlavors();
        //进行非空判断
        if (flavors != null && flavors.size() > 0) {
            //向即将添加的口味表中设置dishId
            flavors.forEach(dishFlavor -> {
                dishFlavor.setDishId(dishId);
            });
            //添加数据
            dishFlavorMapper.insertBatch(flavors);
        }
    }

    //分页查询
    @Override
    public PageResult findByPage(DishPageQueryDTO dishPageQueryDTO) {
        //分页
        PageHelper.startPage(dishPageQueryDTO.getPage(), dishPageQueryDTO.getPageSize());
        List<DishVO> dishVOList = dishMapper.selectByPage(dishPageQueryDTO);
        Page<DishVO> p = (Page<DishVO>) dishVOList;
        return new PageResult(p.getTotal(), p.getResult());
    }

    //删除菜品
    @Override
    @Transactional(rollbackFor = SQLException.class)
    public Integer removeById(List<Long> ids) {
        //1.在售状态下的菜品，不可删除，
        //判断是否为在售状态，在售状态抛出异常
        for (Long id : ids) {
            Dish dish = dishMapper.selectById(id);
            if (dish.getStatus() == StatusConstant.ENABLE) {
                throw new DeletionNotAllowedException(MessageConstant.DISH_ON_SALE);
            }
        }

        //2.被套餐关联的菜品不可删除
        //根据菜品id获取是否关联了套餐
        List<Long> setmealIds = setmealDishMapper.selectSetmealIdsByDishIds(ids);
        if (setmealIds != null && setmealIds.size() > 0) {
            //有数据，关联了，抛出异常
            throw new DeletionNotAllowedException(MessageConstant.DISH_BE_RELATED_BY_SETMEAL);
        }

        //3.删除菜品的同时，应该删除该菜品对应的口味
        Integer tag = dishMapper.deleteByIds(ids);
        //删除该菜品对应的口味列表
        dishFlavorMapper.deleteByDishId(ids);

        //清除缓存
        cleanRedis("dish_*");
        return tag;
    }

    //根据id查询菜品
    @Override
    public DishVO getById(Long id) {
        //获取该菜品的基本数据
        Dish dish = dishMapper.selectById(id);
        //获取该菜品对应的口味列表
        List<DishFlavor> dishFlavorList = dishFlavorMapper.selectByDishId(id);

        //转换为对应VO模型
        DishVO dishVO = new DishVO();
        BeanUtils.copyProperties(dish, dishVO);
        dishVO.setFlavors(dishFlavorList);

        return dishVO;
    }

    //修改菜品
    @Override
    @Transactional
    public Integer modifyDish(DishDTO dishDTO) {
        //修改菜品的基本属性（dish）
        Dish dish = new Dish();
        BeanUtils.copyProperties(dishDTO, dish);
        Integer tag = dishMapper.update(dish);

        //修改菜品对应的口味列表（dish_flavor）
        //1.先删除该菜品的口味列表
        List<Long> dishIdList = new ArrayList<>();
        dishIdList.add(dishDTO.getId());
        dishFlavorMapper.deleteByDishId(dishIdList);

        //2.重新添加
        List<DishFlavor> flavors = dishDTO.getFlavors();
        //为即将新添加的口味列表设置dishId
        if (flavors != null && flavors.size() != 0) {
            flavors.forEach(dishFlavor -> {
                dishFlavor.setDishId(dishDTO.getId());
            });
            //往口味列表中添加数据
            dishFlavorMapper.insertBatch(flavors);
        }

        //清除缓存
        cleanRedis("dish_*");
        return tag;
    }

    //修改状态
    @Override
    public Integer modifyStatus(Integer status, Long id) {
        Dish dish = Dish.builder()
                .id(id)
                .status(status)
                .build();
        //TODO 菜品停售，则包含菜品的套餐同时停售
        //清除缓存
        cleanRedis("dish_*");
        return dishMapper.update(dish);
    }

    //根据分类id查询菜品
    @Override
    public List<DishVO> findByCategoryId(Long categoryId) {
        List<DishVO> dishVOList = dishMapper.selectListByCategoryId(categoryId);
        return dishVOList;
    }

    //根据分类id查询菜品(带口味)
    @Override
    public List<DishVO> findByCategoryIdWithFlavor(Dish dish) {
        //先从缓存中查找数据
        //如果存在,直接返回->不需要从数据库中进行查找,减少数据库访问压力
        String key = "dish_" + dish.getCategoryId();
        Object obj = redisTemplate.opsForValue().get(key);
        //进行判断是非为空
        if (obj != null) {
            log.info("[从redis中读取菜品列表...]");
            List<DishVO> dishVOList = (List<DishVO>) obj;
            return dishVOList;
        }

        //先查询菜品基本信息
        List<Dish> dishList = dishMapper.selectByCategoryId(dish);

        List<DishVO> dishVOList = new ArrayList<>();

        //遍历集合获取每个菜品的口味
        for (Dish d : dishList) {
            //查询每个菜品关联的口味
            List<DishFlavor> dishFlavorList = dishFlavorMapper.selectByDishId(d.getId());
            DishVO dishVO = new DishVO();
            BeanUtils.copyProperties(d, dishVO);
            dishVO.setFlavors(dishFlavorList);
            dishVOList.add(dishVO);
        }

        //将从数据库查找的数据存入redis缓存
        redisTemplate.opsForValue().set(key, dishVOList);
        log.info("[读取数据库 并存入redis...]");
        return dishVOList;
    }

    /**
     * 以下代码不安全
     * 1.在删除某一个key的时候,有可能会突然出现10w条请求直接访问数据库,数据库最多只能同时承受2000条连接,会击溃数据库
     * @param pattern
     */
    //清除缓存
    private void cleanRedis(String pattern) {
        //修改成功后,清除redis缓存,下次查找的时候重新从数据库中搜索,保证数据的一致性
        //1.查找所有的key
        Set keys = redisTemplate.keys(pattern);
        redisTemplate.delete(keys);
    }
}
