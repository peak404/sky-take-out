package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.exception.AccountLockedException;
import com.sky.exception.AccountNotFoundException;
import com.sky.exception.PasswordErrorException;
import com.sky.mapper.EmployeeMapper;
import com.sky.result.PageResult;
import com.sky.service.EmployeeService;
import com.sky.vo.EmployeeVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    /**
     * 员工登录
     *
     * @param employeeLoginDTO
     * @return
     */
    public Employee login(EmployeeLoginDTO employeeLoginDTO) {
        String username = employeeLoginDTO.getUsername();
        String password = employeeLoginDTO.getPassword();

        //1、根据用户名查询数据库中的数据
        Employee employee = employeeMapper.getByUsername(username);

        //2、处理各种异常情况（用户名不存在、密码不对、账号被锁定）
        if (employee == null) {
            //账号不存在
            throw new AccountNotFoundException(MessageConstant.ACCOUNT_NOT_FOUND);
        }

        //密码比对
        //进行md5加密，然后再进行比对
        password = DigestUtils.md5DigestAsHex(password.getBytes());
        if (!password.equals(employee.getPassword())) {
            //请求过来的密码与查询用户名获取到的密码不一致
            //密码错误
            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
        }

        if (employee.getStatus() == StatusConstant.DISABLE) {
            //账号被锁定
            throw new AccountLockedException(MessageConstant.ACCOUNT_LOCKED);
        }

        //3、返回实体对象
        return employee;
    }

    /**
     * 新增员工
     * @param employeeDTO
     * @return
     */
    @Override
    public Integer add(EmployeeDTO employeeDTO) {
        //log.info("[Service线程id:{}]",Thread.currentThread().getId());
        //将DTO模型转换为实体类
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeDTO,employee);

        //设置密码,默认123456(MD5加密)
        employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));

        //设置性别
        if(employee.getSex().equals("1")){
            employee.setSex("男");
        }else if (employee.getSex().equals("2")){
            employee.setSex("女");
        }

        //设置公共业务
        employee.setStatus(StatusConstant.ENABLE);
        //employee.setCreateTime(LocalDateTime.now());
        //employee.setUpdateTime(LocalDateTime.now());

        //从ThreadLocal中获取当前操作人Id
        //employee.setCreateUser(BaseContext.getCurrentId());
        //employee.setUpdateUser(BaseContext.getCurrentId());

        //调用mapper实现新增
        return  employeeMapper.insertEmp(employee);
    }

    @Override
    public PageResult findByPage(EmployeePageQueryDTO employeePageQueryDTO) {
        //基于PageHelper分页查询
        PageHelper.startPage(employeePageQueryDTO.getPage(),employeePageQueryDTO.getPageSize());

        List<Employee> employeeList = employeeMapper.selectByPage(employeePageQueryDTO.getName());
        Page<Employee> p = (Page<Employee>) employeeList;

        return new PageResult(p.getTotal(),p.getResult());
    }

    @Override
    public Integer modifyStatus(Integer status, Long id) {
        //构建Employee对象
        Employee employee = Employee.builder()
                .id(id)
                .status(status)
                //.updateTime(LocalDateTime.now())
                //.updateUser(BaseContext.getCurrentId())
                .build();

        return employeeMapper.update(employee);
    }

    @Override
    public EmployeeVO getById(Integer id) {
        //将查询到的返回值转换为VO模型
        Employee employee = employeeMapper.selectById(id);
        EmployeeVO employeeVO =new EmployeeVO();
        BeanUtils.copyProperties(employee,employeeVO);
        return employeeVO;
    }

    @Override
    public Integer modifyEmp(EmployeeDTO employeeDTO) {
        //转成对应实体对象
        Employee employee =new Employee();
        BeanUtils.copyProperties(employeeDTO,employee);

        //设置修改时间和修改人
        //employee.setUpdateTime(LocalDateTime.now());
        //employee.setUpdateUser(BaseContext.getCurrentId());

        return employeeMapper.update(employee);
    }

}
