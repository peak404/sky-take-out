package com.sky.service;

import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;

import java.util.List;

public interface ShoppingCartService {

    //添加购物车
    Integer add(ShoppingCartDTO shoppingCartDTO);

    //查看购物车
    List<ShoppingCart> show();

    //清空购物车
    Integer removeAll();

    //删除购物车中的一个商品
    Integer removeOne(ShoppingCartDTO shoppingCartDTO);
}
