package com.sky.mapper;

import com.sky.anno.AutoFill;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.enumeration.OperationType;
import com.sky.vo.DishVO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DishMapper {

    /**
     * 根据分类id查询菜品数量
     * @param categoryId
     * @return
     */
    @Select("select count(id) from dish where category_id = #{categoryId}")
    Integer countByCategoryId(Long categoryId);

    //新增菜品
    @AutoFill(OperationType.INSERT)
    Integer insertDish(Dish dish);

    //分页查询
    List<DishVO> selectByPage(DishPageQueryDTO dishPageQueryDTO);

    //删除菜品
    Integer deleteByIds(List<Long> ids);

    //根据id查询菜品
    @Select("select * from dish where id=#{id}")
    Dish selectById(Long id);

    //修改菜品
    @AutoFill(OperationType.UPDATE)
    Integer update(Dish dish);

    //根据分类id查询菜品
    @Select("select * from dish where category_id=#{categoryId}")
    List<DishVO> selectListByCategoryId(Long categoryId);

    //根据套餐id查询对应菜品列表
    List<Dish> selectBySetmealId(Long setmealId);

    //根据分类id和状态进行查找菜品
    List<Dish> selectByCategoryId(Dish dish);
}
