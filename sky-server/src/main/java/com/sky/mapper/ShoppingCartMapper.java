package com.sky.mapper;

import com.sky.entity.ShoppingCart;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface ShoppingCartMapper {

    //查询购物车
    List<ShoppingCart> select(ShoppingCart shoppingCart);

    //基于id更新购物车
    @Update("update shopping_cart set number=#{number} where id=#{id}")
    Integer update(ShoppingCart data);

    //添加购物车
    @Insert("insert into shopping_cart (name, image, user_id, dish_id, setmeal_id, dish_flavor, amount, create_time) VALUES " +
            "(#{name},#{image},#{userId},#{dishId},#{setmealId},#{dishFlavor},#{amount},#{createTime})")
    Integer insert(ShoppingCart shoppingCart);

    //清空购物车
    @Delete("delete from shopping_cart where user_id = #{userId}")
    Integer deleteAll(Long userId);

    //删除一个商品
    @Delete("delete from shopping_cart where id=#{id} ")
    Integer deleteOneById(Long id);
}

