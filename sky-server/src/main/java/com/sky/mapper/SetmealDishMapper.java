package com.sky.mapper;

import com.sky.entity.SetmealDish;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SetmealDishMapper {


    //查询菜品是否关联了套餐
    List<Long> selectSetmealIdsByDishIds(List<Long> ids);

    //插入套餐关联的菜品
    void insertBySetmealId(List<SetmealDish> setmealDishes);

    //根据套餐id获取菜品列表
    @Select("select * from setmeal_dish where setmeal_id=#{setmealId}")
    List<SetmealDish> selectDishesBySetmealId(Long setmealId);

    //根据分类id删除关联的菜品
    void deleteBySetmealId(List<Long> setmealIds);

}
