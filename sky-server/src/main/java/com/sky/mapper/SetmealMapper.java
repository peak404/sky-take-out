package com.sky.mapper;

import com.sky.anno.AutoFill;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.enumeration.OperationType;
import com.sky.vo.SetmealVO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SetmealMapper {

    /**
     * 根据分类id查询套餐的数量
     * @param id
     * @return
     */
    @Select("select count(id) from setmeal where category_id = #{categoryId}")
    Integer countByCategoryId(Long id);

    //新增套餐
    @Options(keyProperty = "id",useGeneratedKeys = true)
    @AutoFill(OperationType.INSERT)
    @Insert("insert into setmeal (category_id, name, price, description, image, status, create_time, update_time, create_user, update_user) VALUES " +
            "(#{categoryId},#{name},#{price},#{description},#{image},#{status},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    Integer insert(Setmeal setmeal);

    //分页查询
    List<SetmealVO> selectByPage(SetmealPageQueryDTO setmealPageQueryDTO);

    //根据id查询对应套餐状态
    @Select("select status from setmeal where id = #{id}")
    Integer selectStatusByIds(Long id);

    //根据id删除套餐
    Integer deleteByIds(List<Long> ids);

    //根据id获取套餐基本信息
    @Select("select * from setmeal where id=#{id}")
    Setmeal selectById(Long id);

    //修改套餐
    @AutoFill(OperationType.UPDATE)
    Integer update(Setmeal setmeal);

    //根据分类id和状态进行查找套餐
    List<Setmeal> selectByCategoryId(Setmeal setmeal);
}
