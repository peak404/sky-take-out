package com.sky.mapper;

import com.sky.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {

    //根据openid查询
    @Select("select * from user where openid=#{openid}")
    User selectByOpenId(String openid);

    //新增数据
    @Options(keyProperty = "id",useGeneratedKeys = true)
    @Insert("insert into user (openid, name, phone, sex, id_number, avatar, create_time) VALUES " +
            "(#{openid},#{name},#{phone},#{sex},#{idNumber},#{avatar},#{createTime})")
    void insert(User user);
}

