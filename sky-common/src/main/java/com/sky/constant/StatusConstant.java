package com.sky.constant;

/**
 * 状态常量，启用或者禁用
 */
public class StatusConstant {

    public static final String KEY = "SHOP_STATUS";

    //启用
    public static final Integer ENABLE = 1;

    //禁用
    public static final Integer DISABLE = 0;
}
